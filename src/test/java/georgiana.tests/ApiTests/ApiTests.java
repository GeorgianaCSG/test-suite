package georgiana.tests.ApiTests;

import io.restassured.http.ContentType;
//import jdk.javadoc.internal.doclets.toolkit.Content;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;

//import javax.swing.text.AbstractDocument;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

//import static sun.nio.cs.Surrogate.is;

public class ApiTests {

    @Before
    public void BeforeCreation(){
        int y = 9;
        String x = "{\n" +
                "    \"id\": " +y + ",\n" +
                "    \"category\": {\n" +
                "        \"id\": 0,\n" +
                "        \"name\": \"Lola\",\n" +
                "        \"age\": 2\n" +
                "    },\n" +
                "    \"name\": \"doggie\",\n" +
                "    \"photoUrls\": [\n" +
                "        \"string\"\n" +
                "    ],\n" +
                "    \"tags\": [\n" +
                "        {\n" +
                "            \"id\": 12,\n" +
                "            \"name\": \"TOMY\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"status\": \"available\"\n" +
                "}";
        given().contentType(ContentType.JSON).body(x)
                .when().put("https://petstore.swagger.io/v2/pet");

        given().contentType("application/json")
                .when().get("https://petstore.swagger.io/v2/pet/"+y)
                .then().body("id", is(y))
                .body("category.name", is("Lola"))
                .body("status", is("available"))
                .statusCode(200);

    }


    @Test
    public void jsonParsing(){
        get("https://petstore.swagger.io/v2/pet/9").then()
                .body("tags.findAll {it.id<100}[0].name",
                        equalTo("TOMY"));

    }

    @Test
    public void getResponseData() {
        Response response = given().contentType("application/json").
                when().get("http://petstore.swagger.io/v2/pet/9");

        response.prettyPrint();

        int id = response.path("id");
        assertThat(id, is(9));

        String jsonResponse = response.asString();
        JsonPath jsonPath = new JsonPath(jsonResponse);
        String categoryName = jsonPath.getString("category.name");
        assertThat(categoryName, is("Lola"));
    }

    @Test
    public void additionalData() {
        Response response = given().contentType("application/json").
                when().get("http://petstore.swagger.io/v2/pet/12345");

        // Get all headers
        Headers allHeaders = response.getHeaders();

        // Get a single header value:
        String headerValue = response.getHeader("Content-Type");
        assertThat(headerValue, is("application/json"));

        // Get status code
        int statusCode = response.getStatusCode();
        assertThat(response.getStatusCode(), is(200));
    }



}