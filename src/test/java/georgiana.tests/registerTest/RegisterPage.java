package georgiana.tests.registerTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage {

    private WebDriver driver;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "/html//input[@id='reg_email']")
    private WebElement emailAddress;

    @FindBy(xpath = "/html//input[@id='reg_password']")
    private WebElement password;

    @FindBy(xpath = "//div[@id='customer_login']/div[2]/form[@method='post']" +
            "//input[@name='registration_field_1']")
    private WebElement firstName;

    @FindBy(xpath = "//div[@id='customer_login']/div[2]/form[@method='post']" +
            "//input[@name='registration_field_2']")
    private WebElement lastName;

    @FindBy(xpath = "//div[@id='customer_login']/div[2]/form[@method='post']" +
            "//input[@name='registration_field_3']")
    private WebElement phoneNumber;

    @FindBy(xpath = "//div[@id='customer_login']/div[2]/form[@method='post']" +
            "//input[@name='register']")
    private WebElement sendButton;

    public WebElement getBadEmailMessage() {
        return badEmailMessage;
    }

    @FindBy(xpath = "/html//div[@id='primary']/article//ul[@class='woocommerce-error']/li")
    private WebElement badEmailMessage;


    public WebElement getBadPasswordMessage() {
        return badPasswordMessage;
    }

    @FindBy(xpath = "/html//div[@id='primary']/article/div[@class='entry-thumbnail']//ul[@class='woocommerce-error']")
    private WebElement badPasswordMessage;


    public WebElement getBadFirstNameMessage() {
        return badFirstNameMessage;
    }

    @FindBy(xpath = "/html//div[@id='primary']/article/div[@class='entry-thumbnail']//ul[@class='woocommerce-error']")
    private WebElement badFirstNameMessage;


    public WebElement getBadLastNameMessage() {
        return badFirstNameMessage;
    }

    @FindBy(xpath = "/html//div[@id='primary']/article/div[@class='entry-thumbnail']//ul[@class='woocommerce-error']")
    private WebElement badLastNameMessage;


    public WebElement getBadPhoneMessage() {
        return badPhoneMessage;
    }

    @FindBy(xpath = "/html//div[@id='primary']/article/div[@class='entry-thumbnail']//ul[@class='woocommerce-error']")
    private WebElement badPhoneMessage;


    public HomePageShop formAuthenticate(String userEmail, String userPassword, String userFirstName,
                                         String userLastName, String userPhone) throws InterruptedException {

        emailAddress.sendKeys(userEmail);
        password.sendKeys(userPassword);
        firstName.sendKeys(userFirstName);
        lastName.sendKeys(userLastName);
        phoneNumber.sendKeys(userPhone);

        //Thread.sleep(10000);

        return new HomePageShop(driver);
    }

    public HomePageShop sendFormRegisterBTN() {
        sendButton.click();
        return new HomePageShop(driver);
    }

}
