package georgiana.tests.registerTest;


import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.loader.LoaderType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.persistence.SecondaryTable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

@RunWith(DataProviderRunner.class)
public class LoginDataProvider {

    @DataProvider
    public static Object[][] loginDataProvider() {
        return readcsvFile("./src/test/folderCsv/loginData.csv");
    }

    public static Object[][] readcsvFile(String csvFile){
       // String csvFile = "/Users/mkyong/csv/country.csv";
        String line = "";
        String cvsSplitBy = ",";

        ArrayList<Object[]> dataOutput = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] loginData = line.split(cvsSplitBy);
                dataOutput.add(loginData);

                //System.out.println("Country [code= " + loginData[4] + " , name=" + loginData[5] + "]");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Object[][] outputMatrix = new Object[dataOutput.size()][];
        for(int i =0; i<dataOutput.size(); i++){
            outputMatrix[i] = dataOutput.get(i);
        }
      //return (Object[][]) dataOutput.toArray();
         return outputMatrix;
    }

    @Test
    @UseDataProvider("loginDataProvider")
    public void willTestSomething(String user, String password, boolean isValid) {

        System.out.println(user + password + isValid);
    }

    /* @Test
     @UseDataProvider("loginDataProvider")
    public static willTestLoginWithCSV(){
        HomePage csvHome = new HoomePage(driver);

   }*/

}
