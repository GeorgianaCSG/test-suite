package georgiana.tests.registerTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageShop {

    private WebDriver driver;

    @FindBy(xpath = "//input[@id='username']")
    private WebElement userNameOrEmail;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement password;

    @FindBy(xpath = "//div[@id='customer_login']/div[1]/form[@method='post']//input[@name='login']")
    private WebElement loginBtn;


    public LoginPageShop(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public HomePageShop authenticateFormLogin(String user, String passw){
        userNameOrEmail.sendKeys(user);
        password.sendKeys(passw);
        loginBtn.click();
        return new HomePageShop(driver);
    }

    public HomePageShop sendFormLoginBTN(){
        loginBtn.click();
        return new HomePageShop(driver);
    }

}
