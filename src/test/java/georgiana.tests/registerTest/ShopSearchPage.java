package georgiana.tests.registerTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ShopSearchPage {

    private WebDriver driver;

    public ShopSearchPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//header[@id='masthead']//div[@class='bottom-header-wrapper clearfix']" +
            "//div[@class='tg-container']//div[@class='search-user-wrapper clearfix']" +
            "//div[@class='search-wrapper search-user-block']")
    private WebElement searchButton;
    @FindBy(xpath = "//header[@id='masthead']//div[@class='bottom-header-wrapper clearfix']" +
            "//div[@class='tg-container']//div[@class='search-user-wrapper clearfix']" +
            "//div[@class='search-wrapper search-user-block']//div[@class='header-search-box active']//form//input")
    private WebElement searchField;
    @FindBy(xpath = "//header[@id='masthead']//div[@class='bottom-header-wrapper clearfix']" +
            "//div[@class='tg-container']//div[@class='search-user-wrapper clearfix']" +
            "//div[@class='search-wrapper search-user-block']//div[@class='header-search-box active']//form//button")
    private WebElement searchSubmitButton;


    public HomePageShop firstSearch(String searchText) {
        searchButton.click();
        searchField.sendKeys(searchText);
        searchSubmitButton.click();
        return new HomePageShop(driver);
    }

    public void clearSearchField(){
        searchButton.click();
        searchField.clear();
    }



}
