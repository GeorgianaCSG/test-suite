package georgiana.tests.registerTest;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class RegisterTests extends BaseTestClass {

    private String userEmail = "steve" + RandomStringUtils.randomAlphabetic(5) + "@gmail.com";
    private String emptyUserEmail = "";

    private String userPassword = "MYPassword123@@@";
    private String emptyUserPassword = "";

    private String userFirstName = "Steve";
    private String emptyUserFirstName = "";

    private String userLastName = "King";
    private String emptyUserLastName = "";

    private String userPhone = "1234567890";
    private String emptyUserPhone = "";

    @Before
    public void setupImplicitWait() {

        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void registerWithValidData() throws InterruptedException {
        HomePageShop validHomepage = new HomePageShop(driver);
        validHomepage.RegisterPage();

        RegisterPage positiveAuthentication = new RegisterPage(driver);
        positiveAuthentication.formAuthenticate(userEmail, userPassword, userFirstName, userLastName, userPhone);
        positiveAuthentication.sendFormRegisterBTN();

        Assert.assertEquals("http://practica.wantsome.ro/shop/", driver.getCurrentUrl());

    }


    @Test
    public void registerWithEmptyUserEmail() throws InterruptedException {
        HomePageShop negativeHomePage = new HomePageShop(driver);
        negativeHomePage.RegisterPage();

        RegisterPage negativeEmail = new RegisterPage(driver);
        negativeEmail.formAuthenticate(emptyUserEmail, userPassword, userFirstName, userLastName, userPhone);
        negativeEmail.sendFormRegisterBTN();

        //Assert.assertTrue(negativeEmail.getBadEmailMessage().isDisplayed());
        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/?page_id=560"));

    }

    @Test
    public void registerWithEmptyUserPassword() throws InterruptedException {
        HomePageShop negativeHomePage = new HomePageShop(driver);
        negativeHomePage.RegisterPage();

        RegisterPage negativePassword = new RegisterPage(driver);
        negativePassword.formAuthenticate(emptyUserEmail, emptyUserPassword, userFirstName, userLastName, userPhone);
        negativePassword.sendFormRegisterBTN();

        ///Assert.assertTrue(negativePassword.getBadPasswordMessage().isDisplayed());
        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/?page_id=560"));

    }

    @Test
    public void registerWithEmptyUserFirstName() throws InterruptedException {
        HomePageShop negativeHomePage = new HomePageShop(driver);
        negativeHomePage.RegisterPage();

        RegisterPage negativeFirstName = new RegisterPage(driver);
        negativeFirstName.formAuthenticate(emptyUserEmail, userPassword, emptyUserFirstName, userLastName, userPhone);
        negativeFirstName.sendFormRegisterBTN();

       // Assert.assertTrue(negativeFirstName.getBadFirstNameMessage().isDisplayed());
        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/?page_id=560"));

    }

    @Test
    public void registerWithEmptyLastName() throws InterruptedException {
        HomePageShop negativeHomePage = new HomePageShop(driver);
        negativeHomePage.RegisterPage();

        RegisterPage negativeLastName = new RegisterPage(driver);
        negativeLastName.formAuthenticate(userEmail, userPassword, userFirstName, emptyUserLastName, userPhone);
        negativeLastName.sendFormRegisterBTN();

        //Assert.assertTrue(negativeLastName.getBadLastNameMessage().isDisplayed());
        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/?page_id=560"));

    }


    @Test
    public void registerWithEmptyUserPhone() throws InterruptedException {
        HomePageShop homePage = new HomePageShop(driver);
        homePage.RegisterPage();

        RegisterPage negativePhone = new RegisterPage(driver);
        negativePhone.formAuthenticate(userEmail, userPassword, userFirstName, userLastName, emptyUserPhone);
        negativePhone.sendFormRegisterBTN();

       //Assert.assertTrue(negativePhone.getBadPhoneMessage().isDisplayed());
        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/?page_id=560"));

    }
}

