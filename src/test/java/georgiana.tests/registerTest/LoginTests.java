package georgiana.tests.registerTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class LoginTests extends BaseTestClass{

    private String userNameOrEmail = "automation";
    private String emptyUserNameOrEmail = "";
    private String password = "Wantsome1!";
    private String emptyPassword = "";

    @Before
    public void setupImplicitWait() {

        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);

    }

    @Test
    public void loginWithValidData(){
        HomePageShop validHomepage = new HomePageShop(driver);
        validHomepage.LoginPageShop();

        LoginPageShop loginPage = new LoginPageShop(driver);
        loginPage.authenticateFormLogin(userNameOrEmail, password);
        loginPage.sendFormLoginBTN();

        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/"));

    }
    @Test
    public void loginWithEmptyUserNameOrEmail(){
        HomePageShop validHomepage = new HomePageShop(driver);
        validHomepage.LoginPageShop();

        LoginPageShop loginEmptyUserOrEmail = new LoginPageShop(driver);
        loginEmptyUserOrEmail.authenticateFormLogin(emptyUserNameOrEmail, password);
        loginEmptyUserOrEmail.sendFormLoginBTN();

        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/"));

    }
    @Test
    public void loginWithEmptyPassword(){
        HomePageShop validHomepage = new HomePageShop(driver);
        validHomepage.LoginPageShop();

        LoginPageShop loginEmptyPassword = new LoginPageShop(driver);
        loginEmptyPassword.authenticateFormLogin(userNameOrEmail, emptyPassword);
        loginEmptyPassword.sendFormLoginBTN();

        Assert.assertTrue(driver.getCurrentUrl().contains("http://practica.wantsome.ro/shop/"));

    }
}
